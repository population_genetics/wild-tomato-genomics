# Wild tomato genomics

Scripts and data of the paper

**Local and temporal adaptation to climatic change in a wild tomato species via selective sweeps**<br>
Kai Wei, Gustavo A Silva-Arias, Aurélien Tellier
